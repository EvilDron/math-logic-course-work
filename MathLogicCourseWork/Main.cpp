﻿#pragma comment(lib, "bdd.lib")
#include "bdd.h"
#include <iomanip>
#include <string>

using namespace std;

typedef unsigned int uint;

const int SIDE_LEN = 3;
const int N = SIDE_LEN * SIDE_LEN;
const uint M = 4;
const uint BOOL_PARAM_COUNT = 4;
const uint N_VAR = N * M * BOOL_PARAM_COUNT;

const int K = 18;

bdd p[M][N][N];

bdd topRight(int property1, int val1, int property2, int val2)
{
	bdd prom1 = bddtrue;

	for (int i = 0; i < SIDE_LEN; ++i)
	{
		prom1 &= !p[property1][SIDE_LEN * (SIDE_LEN - 1) + i][val1];
		prom1 &= !p[property1][SIDE_LEN * i][val1];
		prom1 &= !p[property2][SIDE_LEN * (i + 1) - 1][val2];
		prom1 &= !p[property2][i][val2];
	}

	for (int i = 1; i < SIDE_LEN; ++i)
		for (int j = 0; j < SIDE_LEN - 1; ++j)
			prom1 &= !(p[property1][(i - 1) * SIDE_LEN + j + 1][val1] ^ p[property2][i * SIDE_LEN + j][val2]);

	return prom1;
}

bdd bottomLeft(int property1, int val1, int property2, int val2)
{
	bdd prom1 = bddtrue;

	for (int i = 0; i < SIDE_LEN; ++i)
	{
		prom1 &= !p[property1][i][val1];
		prom1 &= !p[property1][(i + 1) * SIDE_LEN - 1][val1];
		prom1 &= !p[property2][i * SIDE_LEN][val2];
		prom1 &= !p[property2][SIDE_LEN * SIDE_LEN - i - 1][val2];
	}

	for (int i = 0; i < SIDE_LEN - 1; ++i)
		for (int j = 1; j < SIDE_LEN; ++j)
			prom1 &= !(p[property1][(i + 1) * SIDE_LEN + (j - 1)][val1] ^ p[property2][i * SIDE_LEN + j][val2]);

	return prom1;
}

void fun(char* varset, int size);

int main()
{
	bdd_init(10000000, 1000000);
	bdd_setvarnum(N_VAR);

	for (uint k = 0; k < M; ++k)
	{
		uint move = 0;
		for (uint i = 0; i < N; ++i)
		{
			for (uint j = 0; j < N; ++j)
			{
				p[k][i][j] = bddtrue;
				for (uint l = 0; l < BOOL_PARAM_COUNT; l++)
					p[k][i][j] &= ((j >> l) & 1)
					? bdd_ithvar(move + BOOL_PARAM_COUNT * k + l)
					: bdd_nithvar(move + BOOL_PARAM_COUNT * k + l);
			}
			move += BOOL_PARAM_COUNT * M;
		}
	}

	bdd data = bddtrue;

	for (uint k = 0; k < M; ++k)
	{
		for (uint i = 0; i < N; ++i)
		{
			bdd temp = bddfalse;

			for (uint j = 0; j < N; j++)
				temp |= p[k][i][j];
			data &= temp;
		}
	}

	data &= p[3][0][2];
	data &= p[2][6][1];
	data &= p[2][4][3];
	data &= p[3][8][7];

	data &= p[1][3][6];
	data &= p[3][2][4];
	data &= p[2][3][5];
	data &= p[1][4][1];
	data &= p[1][0][0];
	data &= p[0][0][4];

	for (unsigned i = 0; i < N; i++)
		data &= !(p[0][i][3] ^ p[1][i][4]);
	for (unsigned i = 0; i < N; i++)
		data &= !(p[1][i][2] ^ p[2][i][2]);
	for (unsigned i = 0; i < N; i++)
		data &= !(p[2][i][1] ^ p[3][i][0]);
	for (unsigned i = 0; i < N; i++)
		data &= !(p[2][i][8] ^ p[1][i][7]);
	for (unsigned i = 0; i < N; i++)
		data &= !(p[1][i][3] ^ p[2][i][7]);

	data &= topRight(0, 8, 0, 0);
	data &= topRight(1, 7, 0, 1);
	data &= bottomLeft(1, 2, 0, 6);
	data &= bottomLeft(3, 6, 0, 2);
	data &= bottomLeft(2, 3, 2, 6);

	int property1, property2, val1, val2;
	property1 = 1, val1 = 8, property2 = 0, val2 = 5;
	data &= topRight(property1, val1, property2, val2) | bottomLeft(property1, val1, property2, val2);
	property1 = 0, val1 = 3, property2 = 0, val2 = 8;
	data &= topRight(property1, val1, property2, val2) | bottomLeft(property1, val1, property2, val2);

	for (int i = 1; i < N - 1; i++) {
		for (int j = 0; j < N; j++) {
			for (int k = 0; k < N; k++) {
				for (int x = 0; x < N; x++) {
					for (int y = 0; y < N; y++) {
						if (j + k + x + y > K) {
							data &= !(p[0][i][j] & p[1][i][k] & p[2][i][x] & p[3][i][y]);
						}
					}
				}

			}
		}
	}

	for (uint k = 0; k < M; ++k) {
		for (uint j = 0; j < N; j++) {
			for (uint i = 0; i < N - 1; i++) {
				for (uint ii = i + 1; ii < N; ii++) {
					data &= p[k][i][j] >> !p[k][ii][j];
				}
			}
		}
	}


	const uint satcount = (uint)bdd_satcount(data);
	cout << satcount << " solution(s) found!";
	if (satcount > 1)
	{
		cout << 's';
	}
	cout << ":\n" << endl;
	if (satcount)
	{
		bdd_allsat(data, fun);
	}

	bdd_done();

	system("pause");
	return 0;
}

char var[N_VAR];

void print()
{
	const uint colT1Len = 10;
	const uint colT2Len = 11;
	const string mesT1 = "Object";
	const string mesT2 = "Property";
	const string div = " | ";

	cout << setw(colT1Len) << mesT1 << div;
	for (uint i = 0; i < M; ++i)
	{
		int numCount = 0;
		int p = i + 1;
		while (p > 0)
		{
			p = p / 10;
			++numCount;
		}

		cout << setw(colT2Len - numCount - 1) << mesT2 << " " << i + 1 << div;
	}
	cout << endl;

	for (uint i = 0; i < N; i++)
	{
		cout << setw(colT1Len) << i << div;
		for (uint j = 0; j < M; j++)
		{
			int J = i * M * BOOL_PARAM_COUNT + j * BOOL_PARAM_COUNT;
			int num = 0;
			for (unsigned k = 0; k < BOOL_PARAM_COUNT; k++)
				num += (uint)(var[J + k] << k);
			cout << setw(colT2Len) << num << div;
		}
		cout << endl;
	}
	cout << endl;
}

void build(char* varset, unsigned n, unsigned I)
{
	if (I == n - 1)
	{
		if (varset[I] >= 0)
		{
			var[I] = varset[I];
			print();
			return;
		}
		var[I] = 0;
		print();
		var[I] = 1;
		print();
		return;
	}
	if (varset[I] >= 0)
	{
		var[I] = varset[I];
		build(varset, n, I + 1);
		return;
	}
	var[I] = 0;
	build(varset, n, I + 1);
	var[I] = 1;
	build(varset, n, I + 1);
}

void fun(char* varset, int size)
{
	build(varset, size, 0);
}
